module.exports = {
	port: process.env.PORT || 3000,
	database: process.env.DATABASE_URL || "mongodb+srv://admin:1234@cluster0-wmvh9.mongodb.net/taskr?retryWrites=true&w=majority",
	secret: process.env.SECRET_KEY || "taskr@S3CR3T"
}