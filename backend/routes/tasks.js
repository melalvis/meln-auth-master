const express = require('express');
const mongoose = require('mongoose');

const TaskModel = require('../models/Task');
const auth = require('../middlewares/auth');
const admin = require('../middlewares/admin')

const router = express.Router();

router.get('/', async (req, res)=> {

	let tasks = await TaskModel.find();
	res.send(tasks);
});

router.post('/', [auth, admin], async (req, res)=> {

	let { taskName, subtasks, created_at, isDone } = req.body;
	
	let task = new TaskModel ({
		
		taskName: req.body.task,
		user: _idreq.user._id
	});

	task = await task.save();

	res.send(task);

});

router.put('/:id', async (req, res) => {
	let task = await TaskModel.findByIdAndUpdate(req.params.id, {
		taskName: req.body.task

	}, {new: true});

	res.send(task);
});

router.delete('/:id', async (req, res) => {

	let task = await TaskModel.findByIdAndRemove(req.params.id);
	res.send(task);

});




router.post('/:taskId/subtasks', async (req, res) => {
	//find
	let task = await TaskModel.findById(req.params.taskId);
	//create objet subtask
	let subTask = {
		taskName: req.body.task,
	
	}
	//push the object to subtask
	task.subTasks.push(subTask);
	//save
	task = await task.save();

	res.send(task);

});

router.post('tasks')

module.exports = router;
