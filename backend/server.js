const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const app = express();
const tasks = require('./routes/tasks');
const users = require('./routes/users');
const auth = require('./routes/auth');

const config = require('./config');

app.use(cors ());



mongoose.connect(config.database, { useNewUrlParser: true }).then(() => {
	console.log('remote connection established');
});

app.use(express.json());
app.use(express.urlencoded({extended:true}))


app.use('/api/auth', auth);
app.use('/api/tasks', tasks);
app.use('/api/users', users);

	

app.listen(config.port, () => {
	console.log(`listening on port:${config.port}`);
});