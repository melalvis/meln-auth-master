const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TaskSchema = new Schema({
	
	taskName: String,
	created_at: {type: Date, default: Date.now() },
	isDone: { type: Boolean, default: false },
	subTasks: [
		{
			taskName: String,
			created_at: { type: Date, default: Date.now() },
			isDone: { type: Boolean, default: false }
		}
	],

	//populate
	user: {
		type:mongoose.Schema.Types.ObjectId,
		ref: "User"
	}

});

const Task = mongoose.model('Task', TaskSchema);

module.exports = Task;