<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class Authenticate 
{
    /**
     * Get the path the user should be redirected to when they are not authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return string
     */
    public function handle($request, Closure $next)
    {
        if (!Session::get('user')) {
            return redirect('/login');
        }

        return $next($request);
    }
}
