<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Session;

use GuzzleHttp\Exception\BadResponseException;


class AuthController extends Controller
{
    public function index() {
    	return view('auth.login');
    }

    public function login(Request $request)
    {
    	$client = new Client;
    	
    	try {
    		$response = $client->post('localhost:3000/api/auth', [
    			"json" => [
    			"email" => $request->email,
    			"password" => $request->password
    		]
    	]);

    	} catch (BadResponseException $error) {
    		$response = $error->getResponse();
			$body = $response->getBody();
			$message = json_decode($body->getContents());
			Session::flash('invalidCredentials', $message->message);

			return back();
    	}

    	Session::put('token', $response->getHeader('x-auth-token')[0]);
    	Session::put('user', json_decode($response->getBody()));
   		// dd(json_decode($response->getBody()));

   		return redirect('/');
    }

    public function logout(Request $request)
    	{
    		Session::flush();
    		return redirect('/login');
    	}

}
